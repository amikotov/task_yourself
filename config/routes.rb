Rails.application.routes.draw do
  devise_for :users, :controllers => { registrations: 'registrations' }

  root 'home#index'
  get '/profile', as: :user_root, controller: :profile, action: :show
  resources :users do
    member do
      resources :tasks, only: [:index]
    end
  end

  resources :tasks, only: [:show, :new, :create, :edit, :update, :destroy] do
    collection do
      get '/search', controller: :tasks, action: :search
    end
  end
end
