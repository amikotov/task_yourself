require 'rails_helper'

RSpec.describe TasksController, type: :controller do
  describe '#search' do
    subject(:search_tasks) { get :search, params: { query: query } }

    let(:query) { generate(:string) }

    it 'successfully search tasks' do
      expect(search_tasks).to be_success
    end
  end

  describe '#new' do
    subject(:new_task) { get :new }

    it 'successfully show new task' do
      expect(new_task).to be_success
    end
  end

  describe '#create' do
    subject(:create_task) { post :create, params: { task: params }  }

    let(:params) do
      ActionController::Parameters.new(
        name: generate(:name),
        assignee_id: generate(:integer).to_s,
        creator_id: generate(:integer).to_s
      ).permit!
    end

    before { allow(::Tasks::Create).to receive(:call).and_return(double(success?: true)) }

    it 'successfully create task' do
      expect(create_task).to redirect_to(user_root_path)
      expect(::Tasks::Create).to have_received(:call).with(params)
    end

    context 'when creates was unsuccessfully' do
      before do
        allow(::Tasks::Create).to receive(:call).and_return(
          double(success?: false, error: generate(:string), task: build(:task))
        )
      end

      it 'show errors' do
        expect(create_task).to have_http_status(409)
        expect(flash[:danger]).to be_present
        expect(::Tasks::Create).to have_received(:call).with(params)
      end
    end
  end

  describe '#edit' do
    subject(:edit_task) { get :edit, params: { id: task.id } }

    let(:task) { create :task }

    it 'successfull show task for edit' do
      expect(edit_task).to be_successful
    end
  end

  describe '#update' do
    subject(:update_task) { put :update, params: { id: task.id, task: params } }

    let(:task) { create :task }
    let!(:params) do
      ActionController::Parameters.new(
        name: generate(:name),
        creator_id: generate(:integer).to_s,
        assignee_id: generate(:integer).to_s,
        id: task.id.to_s
      ).permit!
    end

    before { allow(::Tasks::Update).to receive(:call).and_return(double(success?: true)) }

    it 'successfully update task' do
      expect(update_task).to redirect_to(user_root_path)
      expect(::Tasks::Update).to have_received(:call).with(params)
    end

    context 'when updates was unsuccessfully' do
      before do
        allow(::Tasks::Update).to receive(:call).and_return(
          double(success?: false, error: generate(:string), task: task)
        )
      end

      it 'show errors' do
        expect(update_task).to have_http_status(409)
        expect(flash[:danger]).to be_present
        expect(::Tasks::Update).to have_received(:call).with(params)
      end
    end
  end

  describe '#destroy' do
    subject(:destroy_task) { delete :destroy, params: { id: task.id } }

    let(:task) { create :task }

    it 'successfull destroy task' do
      expect(destroy_task).to redirect_to(user_root_path)
    end

    context 'when destroy was unsuccessfully' do
      before { allow_any_instance_of(Task).to receive(:destroy).and_return(false) }

      it 'show errors' do
        expect(destroy_task).to have_http_status(409)
      end
    end
  end
end
