require 'rails_helper'

RSpec.describe ProfileController, type: :controller do
  describe '#index' do
    subject(:do_request) { get :show }

    context 'when user was not logged in' do
      it 'redirects to sign in page' do
        do_request

        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'when user was logged in' do
      before { sign_in create :user }

      it 'redirects to user page' do
        do_request

        expect(response).to be_success
      end
    end
  end
end
