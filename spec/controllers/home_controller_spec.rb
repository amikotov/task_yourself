require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  describe '#index' do
    subject(:do_request) { get :index }

    it 'responses with correct status' do
      do_request

      expect(response).to be_success
    end
  end
end
