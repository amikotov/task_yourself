require 'rails_helper'

RSpec.describe RegistrationsController, type: :controller do
  before { request.env['devise.mapping'] = Devise.mappings[:user] }

  describe '#create' do
    subject(:create_user) do
      post :create, params:
        {
          user:
          {
            name: generate(:name),
            email: generate(:email),
            password: generate(:string),
            password_confirmation: generate(:string)
          }
        }
    end

    it 'successfully creates user' do
      expect(create_user).to be_success
    end
  end

  describe '#update' do
    subject(:update_user) do
      put :update, params:
        {
          user:
          {
            email: user.email,
            current_password: password,
            password_confirmation: password
          }
        }
    end

    let(:password) { generate(:string) }
    let!(:user) { create(:user, password: password) }

    before { sign_in user }

    it 'redirects to sign in page' do
      expect(update_user).to be_success
    end
  end
end
