require 'rails_helper'

RSpec.describe Task, type: :model do
  describe '#create' do
    subject(:created_task) do
      described_class.create!(
        name: generate(:string),
        creator: create(:user),
        assignee: create(:user)
      )
    end

    it 'successfully create new task with defatul new state' do
      expect(created_task).to be_persisted
      expect(created_task.state).to eq('new')
    end
  end
end
