require 'rails_helper'

RSpec.describe User, type: :model do
  describe '#create' do
    subject(:created_user) do
      described_class.create!(name: generate(:name), email: generate(:email), password: 'password')
    end

    it 'successfully create new user' do
      expect(created_user).to be_persisted
    end
  end
end
