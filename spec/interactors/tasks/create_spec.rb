require 'rails_helper'

RSpec.describe Tasks::Create do
  subject(:creating_result) { described_class.call(params) }

  shared_examples 'creating task with state' do |state|
    let(:creator) { create(:user) }
    let(:assignee) { create(:user) }
    let(:params) do
      {
        name: generate(:string),
        description: generate(:string),
        creator_id: creator.id,
        assignee_id: assignee.id,
        state: state
      }
    end

    it 'successfully create task with correct status' do
      expect(creating_result).to be_success

      expect(creating_result.task).to have_attributes(
        name: params[:name],
        description: params[:description],
        creator: creator,
        assignee: assignee,
        state: state.to_s
      )
    end
  end

  Task.states.each do |state|
    it_behaves_like 'creating task with state', state
  end

  context 'when provides incorrect attributes for task' do
    let(:params) { {} }

    it 'fails and shows errors' do
      expect(creating_result).to be_failure

      expect(creating_result.error).to be_present
    end
  end
end
