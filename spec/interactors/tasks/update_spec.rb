require 'rails_helper'

RSpec.describe Tasks::Update do
  subject(:updating_result) { described_class.call(params) }

  let!(:task) { create(:task) }

  shared_examples 'update task with state' do |state|
    let(:creator) { create(:user) }
    let(:assignee) { create(:user) }
    let(:params) do
      {
        id: task.id,
        name: generate(:string),
        description: generate(:string),
        creator_id: creator.id,
        assignee_id: assignee.id,
        state: state
      }
    end

    it 'successfully create task with correct status' do
      expect(updating_result).to be_success

      expect(updating_result.task).to have_attributes(
        name: params[:name],
        description: params[:description],
        creator: creator,
        assignee: assignee,
        state: state.to_s
      )
    end
  end

  Task.states.each do |state|
    it_behaves_like 'update task with state', state
  end

  context 'when provides incorrect attributes for task' do
    let(:params) { { id: task.id } }

    it 'fails and shows errors' do
      expect(updating_result).to be_failure

      expect(updating_result.error).to be_present
    end
  end
end
