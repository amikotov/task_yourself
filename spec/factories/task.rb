# frozen_string_literal: true

FactoryBot.define do
  factory :task do
    name
    association :creator, factory: :user
    association :assignee, factory: :user
  end
end
