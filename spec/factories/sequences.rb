

FactoryBot.define do
  %w(password string name).each do |name|
    sequence(name) { |n| "#{name}#{n}" }
  end

  sequence :email do |n|
    "person-#{n}@example.com"
  end

  sequence :integer do |n|
    n
  end
end
