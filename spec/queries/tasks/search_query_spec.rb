require 'rails_helper'

RSpec.describe Tasks::SearchQuery do
  subject(:search_result) { described_class.new(query).result }

  let(:task) { create :task }
  let(:query) { task.name }

  it 'finds correctly task' do
    expect(search_result).to contain_exactly(task)
  end
end
