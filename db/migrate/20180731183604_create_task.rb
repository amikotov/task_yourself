class CreateTask < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :name, null: :false
      t.text :description
      t.references :creator, null: false, index: true
      t.references :assignee, null: false, index: true
      t.string :state, null: false
    end
  end
end
