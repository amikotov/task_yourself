# frozen_string_literal: true

module Tasks
  # Class for search tasks by name or description
  class SearchQuery
    attr_reader :query, :page, :per_page

    def initialize(query, page: 1, per_page: 20)
      @query = query
      @page = page
      @per_page = per_page
    end

    def result
      Task
        .where('name LIKE ? OR description LIKE ?', "%#{query}%", "%#{query}%")
        .page(page)
        .per(per_page)
    end
  end
end
