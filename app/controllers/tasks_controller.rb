# frozen_string_literal: true

# Controller for creating and updating tasks
class TasksController < ApplicationController
  def search
    tasks = ::Tasks::SearchQuery.new(params[:query]).result

    render :search, locals: { tasks: tasks, caption: 'Search result' }
  end

  def new
    task = Task.new

    render :new, locals: { task: task }
  end

  def create
    result = ::Tasks::Create.call(task_params)

    if result.success?
      flash[:alert] = 'Task was created successfully'
      redirect_to user_root_path
    else
      flash[:danger] = result.error
      render :new, locals: { task: result.task }, status: :conflict
    end
  end

  def edit
    task = Task.find(params[:id])

    render :edit, locals: { task: task }
  end

  def update
    result = ::Tasks::Update.call(task_params.merge(id: params[:id]))

    if result.success?
      flash[:alert] = 'Task was updated successfully'
      redirect_to user_root_path
    else
      flash[:danger] = result.error
      render :edit, locals: { task: result.task }, status: :conflict
    end
  end

  def destroy
    task = Task.find(params[:id])

    if task.destroy
      flash[:alert] = 'Task was destroyed successfully'
      redirect_to user_root_path
    else
      flash[:danger] = task.errors.full_messages
      render :edit, locals: { task: task }, status: :conflict
    end
  end

  private

  def task_params
    params.require(:task).permit(:name, :description, :creator_id,
                                 :assignee_id, :state)
  end
end
