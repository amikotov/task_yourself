# frozen_string_literal: true

# Controller for user's profile. Accessed only for authenticated users
class ProfileController < ApplicationController
  before_action :authenticate_user!

  def show
    @user = current_user
  end
end
