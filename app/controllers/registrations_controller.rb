# frozen_string_literal: true

# Class for getting additional params when creat and update user
class RegistrationsController < Devise::RegistrationsController
  private

  def sign_up_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation, :current_password)
  end
end
