# frozen_string_literal: true

module Tasks
  # Create new tasks object with provided attributes. Return errors, if failures
  class Update
    include Interactor

    delegate :name, :creator_id, :assignee_id, :description,
             :state, :id, :task, to: :context

    def call
      context.task = update_task

      context.fail!(error: task.errors.full_messages) if task.invalid?
    end

    private

    def update_task
      updated_task = Task.find(id)

      updated_task.tap do |task|
        task.update(
          name: name,
          description: description,
          creator: creator,
          assignee: assignee,
          state: state
        )
      end
    end

    def creator
      User.find_by(id: creator_id)
    end

    def assignee
      User.find_by(id: assignee_id)
    end
  end
end
