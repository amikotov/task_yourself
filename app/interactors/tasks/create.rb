# frozen_string_literal: true

module Tasks
  # Create new tasks object with provided attributes. Return errors, if failures
  class Create
    include Interactor

    delegate :name, :creator_id,
             :assignee_id, :state, :task, :description, to: :context

    def call
      context.task = ::Task.create(
        name: name,
        description: description,
        creator: creator,
        assignee: assignee,
        state: state
      )

      context.fail!(error: task.errors.full_messages) if task.invalid?
    end

    private

    def creator
      User.find_by(id: creator_id)
    end

    def assignee
      User.find_by(id: assignee_id)
    end
  end
end
