# frozen_string_literal: true

# Model for persist task's data
class Task < ApplicationRecord
  include AASM

  validates :name, presence: true
  belongs_to :creator, class_name: 'User'
  belongs_to :assignee, class_name: 'User'

  aasm :state do
    state :new, initial: true
    state :started
    state :finished

    event :start do
      transitions from: :new, to: :started
    end

    event :finish do
      transitions from: :started, to: :finished
    end

    event :reopen do
      transitions from: :finished, to: :new
    end
  end

  def self.states
    aasm(:state).states.map(&:name)
  end
end
