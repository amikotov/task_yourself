# frozen_string_literal: true

# Model for persist user's data
class User < ApplicationRecord
  devise :database_authenticatable,
         :registerable,
         :lockable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable

  has_many :created_tasks,
           foreign_key: :creator_id,
           class_name: 'Task',
           dependent: :restrict_with_exception,
           inverse_of: false
  has_many :assigned_tasks,
           foreign_key: :assignee_id,
           class_name: 'Task',
           dependent: :restrict_with_exception,
           inverse_of: false

  validates :name, presence: true
end
