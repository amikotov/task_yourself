# TaskYourself

Simple application for create and done your tasks.

For starting work with this application:

- Clone repo
- Install dependencies
```
bundle install
```
- Copy database.yml.example and edit it for your settings
```
cp database.yml.example database.yml
```
- Setup database
```
bundle exec rake db:setup
```
- Start server
```
rails s
```
And open browser http://localhost:3000

## Test

Just run ```rspec spec```.
